extern crate proc_macro;
extern crate syn;
use proc_macro::TokenStream;
use quote::quote;
use syn::DeriveInput;

#[proc_macro_derive(CpuCoreCount)]
pub fn derive_cpu_core_cpunt(item: TokenStream) -> TokenStream {
    let ast = syn::parse_macro_input!(item as DeriveInput);
    let numc = num_cpus::get();
    let name = ast.ident;
    let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();
    quote! {
        impl #impl_generics CpuCoreCount for #name #ty_generics #where_clause {
            const CORE_COUNT : usize = #numc;
        }
    }
    .into()
}

// #[proc_macro]
// pub fn cpu_cores(_ : TokenStream) -> TokenStream {
//     let numc = num_cpus::get();
//     println!("Configuring package to use {} cores...", numc);
//     format!("{numc}").parse().unwrap()
// }
