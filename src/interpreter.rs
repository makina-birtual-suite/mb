use std::sync::{Arc, Mutex, RwLock, RwLockReadGuard, RwLockWriteGuard};

use mb_macro::CpuCoreCount;

/// Macro-suported trait that obtains the number of cores at compile-time.
/// We can, because of this, simplify type declarations imensely, and more seamlessly.
pub trait CpuCoreCount {
    const CORE_COUNT: usize;
}

use crate::{
    devices::{self, mmu::MMU},
    threadpool::{ThreadPool, Worker},
};

#[derive(Debug)]
struct RegisterBank<const REG_COUNT: usize, const REG_SIZE: usize>
where
    [(); REG_COUNT * REG_SIZE]:,
{
    bank: [u8; REG_COUNT * REG_SIZE],
}

#[derive(Debug)]
pub struct VmRegisters {
    fst_bank: RegisterBank<5, 8>,
    args_int: RegisterBank<10, 8>,
    args_f64: RegisterBank<10, 8>,
    args_f32: RegisterBank<10, 4>,
    args_ptr: RegisterBank<10, 8>,
    args_tmp: RegisterBank<40, 8>,
    args_svr: RegisterBank<10, 8>,
    args_simd128: RegisterBank<10, 16>,
    args_simd256: RegisterBank<10, 32>,
    args_priv: RegisterBank<10, 8>,
    special: RegisterBank<7, 8>,
}

impl VmRegisters {
    pub fn new() -> Self {
        Self {
            fst_bank: RegisterBank::new(),
            args_int: RegisterBank::new(),
            args_f64: RegisterBank::new(),
            args_f32: RegisterBank::new(),
            args_ptr: RegisterBank::new(),
            args_tmp: RegisterBank::new(),
            args_svr: RegisterBank::new(),
            args_simd128: RegisterBank::new(),
            args_simd256: RegisterBank::new(),
            args_priv: RegisterBank::new(),
            special: RegisterBank::new(),
        }
    }
}

impl<const N: usize, const S: usize> RegisterBank<N, S>
where
    [(); N * S]:,
{
    const fn new() -> Self {
        Self { bank: [0; N * S] }
    }
}

#[derive(CpuCoreCount)]
struct CPU {
    cores: ThreadPool<Worker, { Self::CORE_COUNT }>,
}

impl CPU {
    fn new() -> Self {
        Self {
            cores: ThreadPool::new(),
        }
    }
    const fn get_cpu_core_count() -> usize {
        Self::CORE_COUNT
    }
}

pub struct VM {
    cpu: CPU,
    raw_mem: Ram,
    devices: Arc<Vec<Arc<Mutex<dyn devices::Device>>>>,
}

pub struct Ram {
    mmu: MMU,
    frames: Arc<RwLock<Vec<RwLock<[u8; 4096]>>>>,
}

impl Ram {
    fn new(mem_in_mb: Option<u64>) -> Self {
        let memory = (0..(mem_in_mb.unwrap_or(1) * 256))
            .map(|_| RwLock::new([0u8; 4096]))
            .collect::<Vec<RwLock<[u8; 4096]>>>();
        Self {
            frames: Arc::new(RwLock::new(memory)),
            mmu: MMU::new(),
        }
    }
    fn shallow_clone(&self) -> Self {
        Self {
            mmu: self.mmu.shallow_clone(),
            frames: Arc::clone(&self.frames),
        }
    }
    fn get_page_read<'a>() -> RwLockReadGuard<'a, [u8; 4096]> {
        todo!()
    }
    fn get_page_write<'a>() -> RwLockWriteGuard<'a, [u8; 4096]> {
        todo!()
    }
}

impl VM {
    pub fn new(devices: Arc<Vec<Arc<Mutex<dyn devices::Device>>>>, mem_in_mb: Option<u64>) -> Self {
        let cpu = CPU::new();
        let raw_mem = Ram::new(mem_in_mb);
        Self {
            cpu,
            raw_mem,
            devices,
        }
    }
    pub fn boot(&self, kernel: &[u8]) {}
    pub fn spawn_task(&self, code: &[u8]) {
        self.cpu.cores.execute(move |regs, is_priv| {});
    }
    pub const fn get_cpu_core_count(&self) -> usize {
        CPU::get_cpu_core_count()
    }
}
