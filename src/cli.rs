use clap::{Parser, Subcommand};

#[derive(Parser)]
#[command(about = "mb: Makina Birtual")]
pub struct Cli {
    #[command(subcommand)]
    command: Commands,
}
impl Cli {
    pub fn command(self) -> Commands {
        self.command
    }
}

#[derive(Subcommand, Clone)]
pub enum Commands {
    #[command(alias = "c", long_about = "Compiles an mknasm file into bytecode.")]
    Compile { file: String },
    #[command(
        alias = "r",
        long_about = "Executes a bytecode file. Optionally takes an integer representing the megabytes of memory that the VM should have, defaulting to 1mb"
    )]
    Run { file: String, mem: Option<u64> },
}
