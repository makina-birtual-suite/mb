#[derive(Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Permission(u8);

impl Permission {
    const fn new() -> Self {
        Self(0) 
    }
    const fn enable_read(self) -> Self {
        Self(self.0 | 1) 
    }
    const fn enable_write(self) -> Self {
        Self(self.0 | 2) 
    }
    const fn enable_execute(self) -> Self {
        Self(self.0 | 4) 
    }
    const fn enable_shared(self) -> Self {
        Self(self.0 | 8) 
    }
    const fn enable_priviledged(self) -> Self {
        Self(self.0 | 16) 
    }
    
}

#[derive(Clone, Default, PartialEq)]
pub struct Segment {
    permissions : Permission,
    virt_start  : u64, 
    size        : u64, 
    page_trans  : Vec<(u64, u64, u64)>,
}
/// Contiguous sets of memory (only in the virtual standpoint.)
/// They can only be expanded towards high memory.
impl Segment {
    const PAGE_BYTES : u64 = 4096;
    pub fn new(perms : Permission, virt_start: u64) -> Self {
        Self {permissions: perms, page_trans: vec![], virt_start,..Default::default()}
    }
    pub fn add_page(&mut self, phys: u64, size: u64) {
        if size > Self::PAGE_BYTES {
            panic!("Adding a page with more than {} bytes is forbidden.", Self::PAGE_BYTES);
        }
        if let Some((prev_start,_,prev_size)) = self.page_trans.last() {
            self.page_trans.push((prev_start + prev_size, phys, size));
            self.size += size;
        } else {
            self.size += size;
            self.page_trans.push((self.virt_start, phys, size));
        }
    } 
    pub fn translate_addr(&self, virt_addr : u64) -> Option<u64> {
        if self.virt_start <= virt_addr && virt_addr < self.virt_start + self.size {
            for (init_addr, phys, size) in self.page_trans.iter() {
                if *init_addr <= virt_addr && virt_addr < *init_addr + *size {
                    return Some(*phys);
                }
            }
        }
        None
    } 
}
