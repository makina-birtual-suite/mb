use crate::interpreter::VmRegisters;
use std::{
    fmt::Debug,
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc, Mutex,
    },
    thread::{spawn, JoinHandle},
};

pub type Task = Box<dyn FnOnce(&mut VmRegisters, &mut bool) + Send + 'static>;

pub trait Workable {
    fn worker(id: usize, recver: Arc<Mutex<Receiver<Task>>>) -> Self;
    fn get_thread(&mut self) -> &mut Option<JoinHandle<()>>;
}

#[derive(Debug)]
#[derive(Default)]
pub struct Worker {
    id: usize,
    thread: Option<JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, recver: Arc<Mutex<Receiver<Task>>>) -> Self {
        let handle = spawn(move || {
            let mut v = VmRegisters::new();
            let mut b = false;
            loop {
                let msg = recver.lock().unwrap().recv();
                match msg {
                    Ok(t) => t(&mut v, &mut b),
                    Err(_) => break,
                }
            }
        });
        Self {
            id,
            thread: Some(handle),
        }
    }
}

impl Workable for Worker {
    fn worker(id: usize, recver: Arc<Mutex<Receiver<Task>>>) -> Self {
        Worker::new(id, recver)
    }

    fn get_thread(&mut self) -> &mut Option<JoinHandle<()>> {
        &mut self.thread
    }
}

pub struct ThreadPool<T: Workable + Debug, const NTHREADS: usize> {
    workers: [T; NTHREADS],
    sender: Option<Sender<Task>>,
}

impl<T: Workable + Debug, const N: usize> ThreadPool<T, N> {
    pub fn new() -> Self {
        assert!(N != 0, "Threadpools must have at least 1 worker!");
        let (tx, rx) = channel();
        let recver = Arc::new(Mutex::new(rx));
        let mut workers = Vec::with_capacity(N);
        for i in 0..N {
            workers.push(T::worker(i, Arc::clone(&recver)));
        }
        let workers = workers.try_into().unwrap();
        Self {
            workers,
            sender: Some(tx),
        }
    }
    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce(&mut VmRegisters, &mut bool) + Send + 'static,
    {
        let t = Box::new(f);
        self.sender.as_ref().unwrap().send(t).unwrap();
    }
}
impl<T: Workable + Debug, const N: usize> Drop for ThreadPool<T, N> {
    fn drop(&mut self) {
        drop(self.sender.take());
        for worker in &mut self.workers {
            if let Some(handle) = worker.get_thread().take() {
                handle.join().unwrap();
            }
        }
    }
}
#[cfg(test)]
mod test {
    use std::{
        ops::DerefMut,
        sync::{Arc, Mutex, RwLock},
        time::{Duration, Instant},
    };

    use super::{ThreadPool, Worker};

    #[test]
    fn print_from_mul_threads() {
        const TASK_COUNT: usize = 10;
        let buffer: Arc<RwLock<Vec<Mutex<String>>>> = Arc::new(RwLock::new(Vec::new()));
        {
            let mut inside = buffer.write().unwrap();
            for _ in 0..TASK_COUNT {
                inside.push(Mutex::new(String::new()));
            }
        }
        let now_thread = std::time::Instant::now();
        {
            let threads: ThreadPool<Worker, 16> = ThreadPool::new();
            for i in 0..TASK_COUNT {
                let buffer = Arc::clone(&buffer);
                threads.execute(move |_, _| {
                    let b = buffer;
                    let inside = b.read().unwrap();
                    let mut string = inside[i].lock().unwrap();
                    std::thread::sleep(Duration::from_millis(20));
                    *string.deref_mut() += "Hello from the threadpool!";
                })
            }
        }
        let threaded_benchmark = now_thread.elapsed();
        let mut buffer = Vec::new();
        for _ in 0..TASK_COUNT {
            buffer.push(String::new());
        }

        let now_mono = Instant::now();
        for i in 0..TASK_COUNT {
            std::thread::sleep(Duration::from_millis(20));
            buffer[i] += "Hello from the main thread";
        }
        let mono_benchmark = now_mono.elapsed();
        assert!(mono_benchmark > threaded_benchmark, "The single threaded application was somehow faster: mono:{mono_benchmark:?} vs threaded:{threaded_benchmark:?}");
    }
}
