#![feature(trivial_bounds, generic_const_exprs)]
use std::sync::Arc;

use clap::Parser;
use interpreter::VM;
//mod alloc;
mod cli;
mod devices;
mod interpreter;
mod parser;
mod segment;
mod threadpool;

fn main() -> std::io::Result<()> {
    let cli = cli::Cli::parse();
    match cli.command() {
        cli::Commands::Run { file, mem } => {
            let contents = std::fs::read(file)?;
            let devices = Arc::new(vec![]);
            let vm = VM::new(devices, mem);
            if cfg!(debug_assertions) {
                println!("Configured to use {} cores.", vm.get_cpu_core_count());
            }
            vm.boot(&contents);
        }
        cli::Commands::Compile { file } => {
            println!("[INFO] Processing {file}...");

            println!("[INFO] Done!");
        }
    }
    Ok(())
}
