use std::{collections::VecDeque, time::{Duration, Instant}};


#[derive(Debug)]
pub enum InterruptKind {
    ReturnToKernel,
    Other(String),
}

#[derive(Debug)]
pub struct Interrupt {
    when_called : Instant,
    how_long : Duration,
    what     : InterruptKind,
}

impl Interrupt {
    fn new(how_long : Duration, what : InterruptKind) -> Self {
        Self {
            when_called : Instant::now(),
            how_long,
            what
        }
    }
}

#[derive(Debug)]
pub struct IntCtl {
    interrupts : VecDeque<Interrupt>,    
}

impl IntCtl {
    const fn new() -> Self {
        Self {
            interrupts : VecDeque::new(),
        }
    }
    
    fn interrupt(&mut self, int : Interrupt) {
        self.interrupts.push_back(int)
    }

    fn handle_ints(&mut self) -> Option<Vec<InterruptKind>> {
        if self.interrupts.is_empty() {
            return None;
        }
        let mut res = Vec::new();
        for _ in 0..self.interrupts.len() {
            if let Some(int) = self.interrupts.pop_front() {
                 if int.when_called.elapsed() >= int.how_long {
                    res.push(int.what);
                }
            } 
        }
        if res.is_empty() {
            None
        } else {
            Some(res)
        }
    }
}
