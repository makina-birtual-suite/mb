pub mod mmu;
pub mod keyboard;
pub mod console;
pub mod display;
pub mod intctl;


pub trait Device {
    fn read_from(&self, port : u8) -> u64;
    fn write_to(&mut self, port : u8, data : u64);
    fn start_up(&self);
}
