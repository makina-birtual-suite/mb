use std::{
    ops::Deref,
    sync::{Arc, Mutex, RwLock},
};

use crate::segment::Segment;

pub struct MMU {
    //mem : Arc<RwLock<Vec<u8>>>,
    segments: Arc<RwLock<Vec<(u64, Mutex<Vec<Segment>>)>>>,
}

impl MMU {
    pub fn new() -> Self {
        Self {
            segments: Arc::new(RwLock::new(Vec::new())),
        }
    }
    pub fn shallow_clone(&self) -> Self {
        Self {
            segments: Arc::clone(&self.segments),
        }
    }
    pub fn decode_addr(&self, pid: u64, virt_addr: u64) -> Result<u64, String> {
        let process_idx = {
            self
            .segments
            .read()
            .map_err(|e| format!("[ERROR] Poisoned MMU: {e}"))?
            .binary_search_by(|probe| probe.0.cmp(&virt_addr))
            .map_err(|_| format!("[ERROR] Segfault: Attempted to access adress {virt_addr:#10X} from process {pid}, which does not have such address allocated to it."))?
        };
        let addr = self
            .segments
            .read()
            .map_err(|e| format!("[ERROR] Poisoned MMU: {e}"))?
            .deref()
            .get(process_idx).unwrap()
            .1
            .lock()
            .map_err(|e| format!("[ERROR] Poisoned MMU: {e}"))?
            .iter()
            .filter_map(|x| x.translate_addr(virt_addr))
            .next();
        match addr {
            Some(a) => Ok(a),
            None => Err(format!("[ERROR] Segfault: Attempted to access adress {virt_addr:#10X} from process {pid}, which does not have such address allocated to it.")),
        }
    }
}
