mod instruction;
mod macro_expr;
mod reg_code;
mod deprecated;
mod asm;
use winnow::prelude::*;

#[inline(always)]
fn char_p(mut target: char) -> impl FnMut(&mut &str) -> PResult<char> {
    #[inline]
    move |input| target.parse_next(input)
}

#[inline(always)]
fn string_p(mut target: &str) -> impl FnMut(&str) -> PResult<&str> + '_ {
    #[inline]
    move |mut input| target.parse_next(&mut input)
}

pub trait ByteParser {
    fn parse_next(input: &mut &[u8]) -> PResult<Self>
    where
        Self: Sized;
    fn parse_byte(target: u8) -> impl FnMut(&mut &[u8]) -> PResult<Self>
    where
        Self: Sized + From<u8>,
    {
        move |input: &mut &[u8]| {
            let mut f = target;
            let res = f.parse_next(input)?;
            Ok(Self::from(res))
        }
    }
}
