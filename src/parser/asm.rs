use std::{collections::HashMap};

use winnow::{
    ascii::{multispace0},
    PResult, Parser,
};

use super::{instruction::Instruction, macro_expr::Macro, ByteParser};

pub struct Scope<'a> {
    kv_pair: HashMap<Macro<'a>, u128>,
}

pub struct AssembleMeta<'a> {
    scopes: Vec<Scope<'a>>,
}

pub struct MBasm<'a> {
    metadata: AssembleMeta<'a>,
    contents: &'a str,
}

pub enum NeedsContext<'a> {
    No(Instruction),
    Yes(Instruction, Macro<'a>),
}

impl<'a> MBasm<'a> {
    fn parse_once(&mut self) -> PResult<NeedsContext<'a>> {
        todo!()
    }
    fn parse_noop(&mut self) -> PResult<NeedsContext<'a>> {
        (multispace0, "noop", multispace0, ";", multispace0)
            .map(|_| NeedsContext::No(Instruction::Noop))
            .parse_next(&mut self.contents)
    }
    fn parse_yield(&mut self) -> PResult<NeedsContext<'a>> {
        (multispace0, "yld", multispace0, ";", multispace0)
            .map(|_| NeedsContext::No(Instruction::Yield))
            .parse_next(&mut self.contents)
    }
    fn parse_ret(&mut self) -> PResult<NeedsContext<'a>> {
        todo!()
    }
}

impl<'a> Iterator for MBasm<'a> {
    type Item = NeedsContext<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        todo!()
    }
}
