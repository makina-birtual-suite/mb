// use nom::{
//     branch::alt,
//     bytes::complete::tag,
//     character::complete::{alpha1, alphanumeric1, hex_digit1, oct_digit1},
//     combinator::{map, recognize},
//     multi::many0_count,
//     sequence::pair,
//     IResult,
// };

// #[deprecated = "This has been deprecated"]
// #[derive(Debug, PartialEq, PartialOrd)]
// enum MacroOps {
//     Add,
//     Mul,
//     Sub,
//     Div,
//     Mod,
//     Neg,
//     Not,
//     Or,
//     And,
//     Xor,
//     LShft,
//     RShft,
// }

// #[deprecated = "This has been deprecated"]
// impl MacroOps {
//     fn parse_add(input: &str) -> IResult<&str, MacroOps> {
//         let (input, _) = nom::character::complete::char('+')(input)?;
//         Ok((input, Self::Add))
//     }
//     fn parse_sub(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('-'), |_| Self::Sub)(input)
//     }
//     fn parse_mul(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('*'), |_| Self::Mul)(input)
//     }
//     fn parse_div(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('/'), |_| Self::Div)(input)
//     }
//     fn parse_mod(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('%'), |_| Self::Mod)(input)
//     }
//     fn parse_neg(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('~'), |_| Self::Neg)(input)
//     }
//     fn parse_not(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('!'), |_| Self::Not)(input)
//     }
//     fn parse_or(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('|'), |_| Self::Or)(input)
//     }
//     fn parse_and(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('&'), |_| Self::And)(input)
//     }
//     fn parse_xor(input: &str) -> IResult<&str, MacroOps> {
//         map(nom::character::complete::char('^'), |_| Self::Xor)(input)
//     }
//     fn parse_lshift(input: &str) -> IResult<&str, MacroOps> {
//         map(tag("<<"), |_| Self::LShft)(input)
//     }
//     fn parse_rshift(input: &str) -> IResult<&str, MacroOps> {
//         map(tag(">>"), |_| Self::RShft)(input)
//     }
//     fn parse(input: &str) -> IResult<&str, MacroOps> {
//         alt((
//             Self::parse_add,
//             Self::parse_sub,
//             Self::parse_mul,
//             Self::parse_div,
//             Self::parse_neg,
//             Self::parse_not,
//             Self::parse_or,
//             Self::parse_and,
//             Self::parse_xor,
//             Self::parse_lshift,
//             Self::parse_rshift,
//         ))(input)
//     }
// }

// #[deprecated = "This has been deprecated"]
// #[derive(Debug, PartialEq)]
// enum MacroExpr<'a> {
//     Symbol(&'a str),
//     WhiteSpace,
//     Value(i128),
//     Op(MacroOps),
//     SubExpr(Vec<MacroExpr<'a>>),
// }

// #[deprecated = "This has been deprecated"]
// #[derive(Debug, PartialEq, PartialOrd)]
// enum MacroAst<'a> {
//     Opb {
//         typ: MacroOps,
//         lhs: Option<Box<MacroAst<'a>>>,
//         rhs: Option<Box<MacroAst<'a>>>,
//     },
//     Opm(MacroOps, Option<Box<MacroAst<'a>>>),
//     Symbol(&'a str),
//     Value(i128),
// }

// #[deprecated = "This has been deprecated"]
// impl<'a> MacroExpr<'a> {
//     fn parse_symbol(input: &'a str) -> IResult<&'a str, MacroExpr> {
//         map(
//             recognize(pair(
//                 alt((alpha1, tag("_"))),
//                 many0_count(alt((alphanumeric1, tag("_")))),
//             )),
//             |x| Self::Symbol(x),
//         )(input)
//     }
//     // fn parse_symbol(input: &str) -> IResult<&str, MacroExpr> {
//     //     let (input, (fst, rst)) = nom::sequence::pair(
//     //         nom::branch::alt((
//     //             nom::character::complete::satisfy(|a: char| nom::character::is_alphabetic(a as u8)),
//     //             nom::character::complete::char('_'),
//     //             nom::character::complete::char('$'),
//     //         )),
//     //         nom::multi::fold_many1(
//     //             nom::branch::alt((nom::character::complete::alphanumeric1, tag("_"))),
//     //             String::new,
//     //             |acc, x| acc + x,
//     //         ),
//     //     )(input)?;
//     //     Ok((input, Self::Symbol(format!("{fst}") + &rst)))
//     // }
//     fn parse_ws(input: &'a str) -> IResult<&str, MacroExpr> {
//         let (input, _) = nom::character::complete::multispace1(input)?;
//         Ok((input, Self::WhiteSpace))
//     }
//     fn parse_binary(input: &'a str) -> IResult<&str, i128> {
//         let (input, (_, num)) = nom::sequence::pair(
//             tag("0b"),
//             nom::combinator::cut(nom::combinator::map_res(
//                 nom::multi::fold_many1(alt((tag("0"), tag("1"))), String::new, |acc, x| acc + x),
//                 |x| i128::from_str_radix(&x, 2),
//             )),
//         )(input)?;
//         Ok((input, num))
//     }
//     fn parse_hex(input: &str) -> IResult<&str, i128> {
//         let (input, (_, num)) = nom::sequence::pair(
//             tag("0x"),
//             nom::combinator::cut(nom::combinator::map_res(hex_digit1, |x| {
//                 i128::from_str_radix(x, 16)
//             })),
//         )(input)?;
//         Ok((input, num))
//     }
//     fn parse_oct(input: &str) -> IResult<&str, i128> {
//         let (input, (_, num)) = nom::sequence::pair(
//             tag("0o"),
//             nom::combinator::cut(nom::combinator::map_res(oct_digit1, |x| {
//                 i128::from_str_radix(x, 8)
//             })),
//         )(input)?;
//         Ok((input, num))
//     }
//     fn parse_value(input: &'a str) -> IResult<&str, MacroExpr> {
//         map(
//             alt((
//                 Self::parse_binary,
//                 Self::parse_oct,
//                 Self::parse_hex,
//                 nom::character::complete::i128,
//             )),
//             |x| MacroExpr::Value(x),
//         )(input)
//     }
//     fn parse_op(input: &'a str) -> IResult<&str, MacroExpr> {
//         map(MacroOps::parse, |x| Self::Op(x))(input)
//     }
//     fn parse(input: &'a str) -> IResult<&str, MacroExpr> {
//         map(
//             nom::sequence::delimited(
//                 tag("$("),
//                 nom::multi::many0(alt((
//                     Self::parse_value,
//                     Self::parse_symbol,
//                     Self::parse_op,
//                     Self::parse_ws,
//                     Self::parse,
//                 ))),
//                 tag(")"),
//             ),
//             |x| MacroExpr::SubExpr(x),
//         )(input)
//     }

//     fn clear_ws(self) -> Self {
//         match self {
//             Self::SubExpr(e) => {
//                 let mut res = Vec::new();
//                 for i in e {
//                     let eval = i.clear_ws();
//                     if eval != Self::WhiteSpace {
//                         res.push(eval);
//                     }
//                 }
//                 Self::SubExpr(res)
//             }
//             a => a,
//         }
//     }
// }

// #[deprecated = "This has been deprecated"]
// #[derive(Debug)]
// enum AstErr {
//     AdjacentValues,
//     NotEnoughArgs,
//     Empty,
//     NonConverging,
//     Invalid,
// }

// #[deprecated = "This has been deprecated"]
// impl<'a> MacroAst<'a> {
//     fn penetrate(&mut self, v: Self) -> Result<(), AstErr> {
//         match self {
//             MacroAst::Opb {
//                 typ: _,
//                 lhs: _,
//                 rhs,
//             } => match rhs {
//                 Some(e) => e.penetrate(v),
//                 None => {
//                     *rhs = Some(Box::new(v));
//                     Ok(())
//                 }
//             },
//             MacroAst::Opm(_, rhs) => match rhs {
//                 Some(e) => e.penetrate(v),
//                 None => {
//                     *rhs = Some(Box::new(v));
//                     Ok(())
//                 }
//             },
//             MacroAst::Symbol(_) => Err(AstErr::AdjacentValues),
//             MacroAst::Value(_) => Err(AstErr::AdjacentValues),
//         }
//     }
//     fn new(input: Vec<MacroExpr<'a>>) -> Result<Self, AstErr> {
//         let mut parsing_stack = Vec::new();
//         for i in input {
//             match i {
//                 MacroExpr::Symbol(s) => {
//                     if parsing_stack.is_empty() {
//                         parsing_stack.push(Self::Symbol(s));
//                     } else {
//                         match parsing_stack.pop().unwrap() {
//                             MacroAst::Opb { typ, lhs, rhs } => match rhs {
//                                 Some(mut e) => (&mut e).penetrate(MacroAst::Symbol(s))?,
//                                 None => parsing_stack.push(Self::Opb {
//                                     typ,
//                                     lhs,
//                                     rhs: Some(Box::new(MacroAst::Symbol(s))),
//                                 }),
//                             },
//                             MacroAst::Opm(op, v) => match v {
//                                 Some(_) => return Err(AstErr::AdjacentValues),
//                                 None => parsing_stack
//                                     .push(Self::Opm(op, Some(Box::new(MacroAst::Symbol(s))))),
//                             },
//                             MacroAst::Symbol(_) => return Err(AstErr::AdjacentValues),
//                             MacroAst::Value(_) => return Err(AstErr::AdjacentValues),
//                         }
//                     }
//                 }
//                 MacroExpr::WhiteSpace => return Err(AstErr::Invalid),
//                 MacroExpr::Value(s) => {
//                     if parsing_stack.is_empty() {
//                         parsing_stack.push(Self::Value(s));
//                     } else {
//                         match parsing_stack.pop().unwrap() {
//                             MacroAst::Opb { typ, lhs, rhs } => match rhs {
//                                 Some(_) => return Err(AstErr::AdjacentValues),
//                                 None => parsing_stack.push(Self::Opb {
//                                     typ,
//                                     lhs,
//                                     rhs: Some(Box::new(MacroAst::Value(s))),
//                                 }),
//                             },
//                             MacroAst::Opm(op, v) => match v {
//                                 Some(_) => return Err(AstErr::AdjacentValues),
//                                 None => parsing_stack
//                                     .push(Self::Opm(op, Some(Box::new(MacroAst::Value(s))))),
//                             },
//                             MacroAst::Symbol(_) => return Err(AstErr::AdjacentValues),
//                             MacroAst::Value(_) => return Err(AstErr::AdjacentValues),
//                         }
//                     }
//                 }
//                 MacroExpr::Op(op) => match op {
//                     MacroOps::Neg | MacroOps::Not => {
//                         if parsing_stack.is_empty() {
//                             parsing_stack.push(MacroAst::Opm(op, None))
//                         } else {
//                             match parsing_stack.pop().unwrap() {
//                                 MacroAst::Opb { typ, lhs, rhs } => match rhs {
//                                     Some(mut e) => (&mut e).penetrate(MacroAst::Opm(op, None))?,
//                                     None => parsing_stack.push(MacroAst::Opb {
//                                         typ,
//                                         lhs,
//                                         rhs: Some(Box::new(MacroAst::Opm(op, None))),
//                                     }),
//                                 },
//                                 _ => return Err(AstErr::AdjacentValues),
//                             }
//                         }
//                     }
//                     _ => {
//                         if parsing_stack.is_empty() {
//                             return Err(AstErr::NotEnoughArgs);
//                         }
//                         match parsing_stack.pop().unwrap() {
//                             MacroAst::Opb { typ, lhs, rhs } => match rhs {
//                                 Some(_) => parsing_stack.push(Self::Opb {
//                                     typ: op,
//                                     lhs: Some(Box::new(Self::Opb { typ, lhs, rhs })),
//                                     rhs: None,
//                                 }),
//                                 None => return Err(AstErr::NotEnoughArgs),
//                             },
//                             MacroAst::Opm(_, None) => return Err(AstErr::NotEnoughArgs),
//                             e => parsing_stack.push(Self::Opb {
//                                 typ: op,
//                                 lhs: Some(Box::new(e)),
//                                 rhs: None,
//                             }),
//                         }
//                     }
//                 },
//                 MacroExpr::SubExpr(sub) => {
//                     if parsing_stack.is_empty() {
//                         parsing_stack.push(MacroAst::new(sub)?)
//                     } else {
//                         match parsing_stack.pop().unwrap() {
//                             MacroAst::Opb { typ, lhs, rhs } => match rhs {
//                                 Some(mut e) => (&mut e).penetrate(MacroAst::new(sub)?)?,
//                                 None => parsing_stack.push(Self::Opb {
//                                     typ,
//                                     lhs,
//                                     rhs: Some(Box::new(MacroAst::new(sub)?)),
//                                 }),
//                             },
//                             MacroAst::Opm(op, rhs) => match rhs {
//                                 Some(mut e) => (&mut e).penetrate(MacroAst::new(sub)?)?,
//                                 None => parsing_stack
//                                     .push(MacroAst::Opm(op, Some(Box::new(MacroAst::new(sub)?)))),
//                             },
//                             _ => return Err(AstErr::AdjacentValues),
//                         }
//                     }
//                 }
//             }
//         }

//         if parsing_stack.len() == 1 {
//             let res = parsing_stack.pop().unwrap();
//             Ok(res)
//         } else if parsing_stack.is_empty() {
//             Err(AstErr::Empty)
//         } else {
//             Err(AstErr::NonConverging)
//         }
//     }
// }

// #[deprecated = "This has been deprecated"]
// #[cfg(test)]
// mod tests {
//     use crate::parser::deprecated::MacroOps;

//     use super::MacroAst;
//     use super::MacroExpr;
//     
//     #[deprecated = "This has been deprecated"]
//     #[test]
//     fn sym_p() {
//         let t1 = MacroExpr::parse_symbol("HELLO_ World!");
//         assert_eq!(
//             t1,
//             nom::IResult::Ok((" World!", MacroExpr::Symbol("HELLO_")))
//         );
//         let t2 = MacroExpr::parse_symbol("H3LL0__BI7CH_ World!");
//         assert_eq!(
//             t2,
//             nom::IResult::Ok((" World!", MacroExpr::Symbol("H3LL0__BI7CH_")))
//         );
//         let t3 = MacroExpr::parse_symbol("__H3LL0__BI7CH_ World!");
//         assert_eq!(
//             t3,
//             nom::IResult::Ok((" World!", MacroExpr::Symbol("__H3LL0__BI7CH_")))
//         );
//         let t4 = MacroExpr::parse_symbol("HELLO World!");
//         assert_eq!(
//             t4,
//             nom::IResult::Ok((" World!", MacroExpr::Symbol("HELLO")))
//         );
//         let t5 = nom::sequence::tuple((
//             MacroExpr::parse_symbol,
//             MacroExpr::parse_ws,
//             MacroExpr::parse_symbol,
//         ))("__H3LL0__BI7CH_ World!");
//         assert_eq!(
//             t5,
//             nom::IResult::Ok((
//                 "!",
//                 (
//                     MacroExpr::Symbol("__H3LL0__BI7CH_"),
//                     MacroExpr::WhiteSpace,
//                     MacroExpr::Symbol("World")
//                 )
//             ))
//         );
//         let t6 = MacroExpr::parse_symbol("_1Hello");
//         assert_eq!(t6, nom::IResult::Ok(("", MacroExpr::Symbol("_1Hello"))));
//     }
//     #[deprecated = "This has been deprecated"]
//     #[test]
//     fn test_expr() {
//         let test_str = "$(30+ 0x5677 * $(~_1OL / $(h1_hi)));";
//         assert_eq!(
//             nom::IResult::Ok((
//                 ";",
//                 MacroExpr::SubExpr(vec![
//                     MacroExpr::Value(30),
//                     MacroExpr::Op(MacroOps::Add),
//                     MacroExpr::Value(22135),
//                     MacroExpr::Op(MacroOps::Mul),
//                     MacroExpr::SubExpr(vec![
//                         MacroExpr::Op(MacroOps::Neg),
//                         MacroExpr::Symbol("_1OL"),
//                         MacroExpr::Op(MacroOps::Div),
//                         MacroExpr::SubExpr(vec![MacroExpr::Symbol("h1_hi")])
//                     ])
//                 ])
//             )),
//             MacroExpr::parse(test_str).map(|(x, y)| (x, MacroExpr::clear_ws(y)))
//         );
//     }
//     #[deprecated = "This has been deprecated"]
//     #[test]
//     fn test_ast() {
//         let test_str = "$(30+ 0x5677 * $(~_1OL / $(h1_hi)));";
//         //let test_str = "$(30+ 0x5677 * $(~_1OL / $(h1_hi)));";
//         let parsed = MacroExpr::parse(test_str).map(|(x, y)| (x, y.clear_ws()));
//         match parsed {
//             Ok((_, p)) => match p {
//                 e => {
//                     assert!(true, "{:#?}", MacroAst::new(vec![e]));
//                 }
//             },
//             Err(e) => assert!(false, "{:?}", e),
//         }
//     }
// }
// fn get_discriminant(&self) -> u8 {
    //     match self {
    //         Instruction::Noop => 0x00,
    //         Instruction::Yield => 0x01,
    //         Instruction::Ret => 0x02,
    //         Instruction::Sched(_, _) => 0x03,
    //         Instruction::Int(_) => 0x04,
    //         Instruction::NoCs => 0x5,
    //         Instruction::Li8(_, _) => 0x6,
    //         Instruction::Li16(_, _) => 0x7,
    //         Instruction::Li32(_, _) => 0x8,
    //         Instruction::Li64(_, _) => 0x9,
    //         Instruction::Li128(_, _) => 0xA,
    //         Instruction::Jmp(_) => 0xB,
    //         Instruction::Jne(_) => 0xC,
    //         Instruction::Jeq(_) => 0xD,
    //         Instruction::Jlt(_) => 0xE,
    //         Instruction::Jge(_) => 0xF,
    //         Instruction::Jltu(_) => 0x10,
    //         Instruction::Jgeu(_) => 0x11,
    //         Instruction::Jez(_) => 0x12,
    //         Instruction::Jnz(_) => 0x13,
    //         Instruction::Call(_) => 0x14,
    //         Instruction::SysCall(_) => 0x15,
    //         Instruction::NativeCall(_) => 0x16,
    //         Instruction::Not(_, _) => 0x17,
    //         Instruction::Or(_, _, _) => 0x18,
    //         Instruction::And(_, _, _) => 0x19,
    //         Instruction::Xor(_, _, _) => 0x1A,
    //         Instruction::Shl(_, _, _) => 0x1B,
    //         Instruction::Shr(_, _, _) => 0x1C,
    //         Instruction::Ori8(_, _, _) => 0x1D,
    //         Instruction::Ori16(_, _, _) => 0x1E,
    //         Instruction::Ori32(_, _, _) => 0x1F,
    //         Instruction::Ori64(_, _, _) => 0x20,
    //         Instruction::Ori128(_, _, _) => 0x21,
    //         Instruction::Andi8(_, _, _) => 0x22,
    //         Instruction::Andi16(_, _, _) => 0x23,
    //         Instruction::Andi32(_, _, _) => 0x24,
    //         Instruction::Andi64(_, _, _) => 0x25,
    //         Instruction::Andi128(_, _, _) => 0x26,
    //         Instruction::Xori8(_, _, _) => 0x27,
    //         Instruction::Xori16(_, _, _) => 0x28,
    //         Instruction::Xori32(_, _, _) => 0x29,
    //         Instruction::Xori64(_, _, _) => 0x2A,
    //         Instruction::Xori128(_, _, _) => 0x2B,
    //         Instruction::Shli8(_, _, _) => 0x2C,
    //         Instruction::Shli16(_, _, _) => 0x2D,
    //         Instruction::Shli32(_, _, _) => 0x2E,
    //         Instruction::Shli64(_, _, _) => 0x2F,
    //         Instruction::Shli128(_, _, _) => 0x30,
    //         Instruction::Shri8(_, _, _) => 0x31,
    //         Instruction::Shri16(_, _, _) => 0x32,
    //         Instruction::Shri32(_, _, _) => 0x33,
    //         Instruction::Shri64(_, _, _) => 0x34,
    //         Instruction::Shri128(_, _, _) => 0x35,
    //         Instruction::Add(_, _, _) => 0x36,
    //         Instruction::Sub(_, _, _) => 0x37,
    //         Instruction::Mul(_, _, _) => 0x38,
    //         Instruction::Div(_, _, _) => 0x39,
    //         Instruction::Mod(_, _, _) => 0x3A,
    //         Instruction::Addi8(_, _, _) => 0x3B,
    //         Instruction::Addi16(_, _, _) => 0x3C,
    //         Instruction::Addi32(_, _, _) => 0x3D,
    //         Instruction::Addi64(_, _, _) => 0x3E,
    //         Instruction::Addi128(_, _, _) => 0x3F,
    //         Instruction::Subi8(_, _, _) => 0x40,
    //         Instruction::Subi16(_, _, _) => 0x41,
    //         Instruction::Subi32(_, _, _) => 0x42,
    //         Instruction::Subi64(_, _, _) => 0x43,
    //         Instruction::Subi128(_, _, _) => 0x44,
    //         Instruction::Muli8(_, _, _) => 0x45,
    //         Instruction::Muli16(_, _, _) => 0x46,
    //         Instruction::Muli32(_, _, _) => 0x47,
    //         Instruction::Muli64(_, _, _) => 0x48,
    //         Instruction::Muli128(_, _, _) => 0x49,
    //         Instruction::Divi8(_, _, _) => 0x4A,
    //         Instruction::Divi16(_, _, _) => 0x4B,
    //         Instruction::Divi32(_, _, _) => 0x4C,
    //         Instruction::Divi64(_, _, _) => 0x4D,
    //         Instruction::Divi128(_, _, _) => 0x4E,
    //         Instruction::LD8(_, _) => 0x4F,
    //         Instruction::SV8(_, _) => 0x50,
    //         Instruction::LD16(_, _) => 0x51,
    //         Instruction::SV16(_, _) => 0x52,
    //         Instruction::LD32(_, _) => 0x53,
    //         Instruction::SV32(_, _) => 0x54,
    //         Instruction::LD64(_, _) => 0x55,
    //         Instruction::SV64(_, _) => 0x56,
    //         Instruction::LD128(_, _) => 0x57,
    //         Instruction::SV128(_, _) => 0x58,
    //         Instruction::Addf32(_, _, _) => 0x59,
    //         Instruction::Addf64(_, _, _) => 0x5A,
    //         Instruction::Subf32(_, _, _) => 0x5B,
    //         Instruction::Subf64(_, _, _) => 0x5C,
    //         Instruction::Mulf32(_, _, _) => 0x5D,
    //         Instruction::Mulf64(_, _, _) => 0x5E,
    //         Instruction::Divf32(_, _, _) => 0x5F,
    //         Instruction::Divf64(_, _, _) => 0x60,
    //         Instruction::Fmadd32(_, _, _, _) => 0x61,
    //         Instruction::Fmadd64(_, _, _, _) => 0x62,
    //         Instruction::ITF64(_, _) => 0x63,
    //         Instruction::ITF32(_, _) => 0x64,
    //         Instruction::FTI64(_, _) => 0x65,
    //         Instruction::FTI32(_, _) => 0x66,
    //     }
    // }
