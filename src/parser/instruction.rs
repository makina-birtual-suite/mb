
use super::{reg_code::*, ByteParser};
use winnow::{
    binary::{u128, u16, u32, u64, u8, Endianness},
    combinator::{alt, cut_err},
    PResult, Parser,
};

#[repr(u8)]
#[derive(Debug, PartialEq, PartialOrd, Eq, Ord)]
pub enum Instruction {
    // * Sys-state relevant instructions
    /// Advances program counter without changing any other state.
    Noop,
    /// yld -> yields priority from the current process to another one.
    Yield,
    /// ret <syntax sugar> returns from a function, based on the 128 bits following the base
    /// pointer. The first 64 are for the new bp, and the next for the new sp.
    Ret,
    /// *RING 0* Schedules an interrupt. sched <code : macro % u32> <time : reg>
    Sched(u32, Regcode),
    /// *RING 0 or during other instruction* Immediatly perform an interrupt.
    Int(u32),
    /// *RING 0* Prevent Scheduler interruption (no context switch)
    NoCs,
    // * Load Immediate
    Li8(Regcode, u8),     // dest_reg <Macro>
    Li16(Regcode, u16),   // dest_reg <Macro>
    Li32(Regcode, u32),   // dest_reg <Macro>
    Li64(Regcode, u64),   // dest_reg <Macro>
    Li128(Regcode, u128), // dest_reg <Macro>
    // * Jumps. These are the underlying instructions for the Jumps. There may be more on the front
    // end that get de-sugared into more simple instructions, such as the call Instruction not
    // being like the one here described.
    Jmp(u64), // <Macro for Addr>
    Jne(u64), 
    Jeq(u64), 
    Jlt(u64), 
    Jge(u64),
    Jltu(u64),
    Jgeu(u64),
    Jez(u64),
    Jnz(u64),
    Call(u64),
    SysCall(u64),
    NativeCall(u64),
    // * Binary Operations
    Not(Regcode, Regcode),         // dest_reg, src_reg
    Or(Regcode, Regcode, Regcode), // dst src src
    And(Regcode, Regcode, Regcode),
    Xor(Regcode, Regcode, Regcode),
    Shl(Regcode, Regcode, Regcode),
    Shr(Regcode, Regcode, Regcode),
    // ** With Immediate
    Ori8(Regcode, Regcode, u8),
    Ori16(Regcode, Regcode, u16),
    Ori32(Regcode, Regcode, u32),
    Ori64(Regcode, Regcode, u64),
    Ori128(Regcode, Regcode, u128),
    //
    Andi8(Regcode, Regcode, u8),
    Andi16(Regcode, Regcode, u16),
    Andi32(Regcode, Regcode, u32),
    Andi64(Regcode, Regcode, u64),
    Andi128(Regcode, Regcode, u128),
    //
    Xori8(Regcode, Regcode, u8),
    Xori16(Regcode, Regcode, u16),
    Xori32(Regcode, Regcode, u32),
    Xori64(Regcode, Regcode, u64),
    Xori128(Regcode, Regcode, u128),
    //
    Shli8(Regcode, Regcode, u8),
    Shli16(Regcode, Regcode, u16),
    Shli32(Regcode, Regcode, u32),
    Shli64(Regcode, Regcode, u64),
    Shli128(Regcode, Regcode, u128),
    //
    Shri8(Regcode, Regcode, u8),
    Shri16(Regcode, Regcode, u16),
    Shri32(Regcode, Regcode, u32),
    Shri64(Regcode, Regcode, u64),
    Shri128(Regcode, Regcode, u128),
    // * Arithmetic
    Add(Regcode, Regcode, Regcode),
    Sub(Regcode, Regcode, Regcode),
    Mul(Regcode, Regcode, Regcode),
    Div(Regcode, Regcode, Regcode),
    Mod(Regcode, Regcode, Regcode),
    // ** With Immediate (except Mod, as it is not common enough)
    Addi8(Regcode, Regcode, u8),
    Addi16(Regcode, Regcode, u16),
    Addi32(Regcode, Regcode, u32),
    Addi64(Regcode, Regcode, u64),
    Addi128(Regcode, Regcode, u128), // SIMD
    //
    Subi8(Regcode, Regcode, u8),
    Subi16(Regcode, Regcode, u16),
    Subi32(Regcode, Regcode, u32),
    Subi64(Regcode, Regcode, u64),
    Subi128(Regcode, Regcode, u128), // SIMD
    //
    Muli8(Regcode, Regcode, u8),
    Muli16(Regcode, Regcode, u16),
    Muli32(Regcode, Regcode, u32),
    Muli64(Regcode, Regcode, u64),
    Muli128(Regcode, Regcode, u128), // SIMD
    //
    Divi8(Regcode, Regcode, u8),
    Divi16(Regcode, Regcode, u16),
    Divi32(Regcode, Regcode, u32),
    Divi64(Regcode, Regcode, u64),
    Divi128(Regcode, Regcode, u128), // SIMD
    // * MEMORY
    LD8(Regcode, Regcode),
    SV8(Regcode, Regcode),
    LD16(Regcode, Regcode),
    SV16(Regcode, Regcode),
    LD32(Regcode, Regcode),
    SV32(Regcode, Regcode),
    LD64(Regcode, Regcode),
    SV64(Regcode, Regcode),
    LD128(Regcode, Regcode), // SIMD
    SV128(Regcode, Regcode), // SIMD
    // * Floating Point
    Addf32(Regcode, Regcode, Regcode),
    Addf64(Regcode, Regcode, Regcode),
    Subf32(Regcode, Regcode, Regcode),
    Subf64(Regcode, Regcode, Regcode),
    Mulf32(Regcode, Regcode, Regcode),
    Mulf64(Regcode, Regcode, Regcode),
    Divf32(Regcode, Regcode, Regcode),
    Divf64(Regcode, Regcode, Regcode),
    Fmadd32(Regcode, Regcode, Regcode, Regcode),
    Fmadd64(Regcode, Regcode, Regcode, Regcode),
    // * Conversion
    ITF64(Regcode, Regcode),
    ITF32(Regcode, Regcode),
    FTI64(Regcode, Regcode),
    FTI32(Regcode, Regcode),
    // * Checks
    Eq0(Regcode),
    Cmp(Regcode, Regcode),
}

impl Instruction {
    fn get_discriminant(&self) -> u8 {
        unsafe { *<*const _>::from(self).cast() }
    }

    fn parse_noop_bytes(input: &mut &[u8]) -> PResult<Self> {
        let mut p = Self::Noop.get_discriminant();
        p.parse_next(input).map(|_| Self::Noop)
    }
    fn parse_yield_bytes(input: &mut &[u8]) -> PResult<Self> {
        let mut p = Self::Yield.get_discriminant();
        p.parse_next(input).map(|_| Self::Yield)
    }
    fn parse_ret(input: &mut &[u8]) -> PResult<Self> {
        let mut p = Self::Ret.get_discriminant();
        p.parse_next(input).map(|_| Self::Ret)
    }
    fn parse_sched(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Sched(0, Regcode::END).get_discriminant();
        (
            p,
            cut_err(u32(Endianness::Little)),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, code, time)| Self::Sched(code, time))
    }
    fn parse_int(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Int(0).get_discriminant();
        (p, cut_err(u32(Endianness::Little)))
            .parse_next(input)
            .map(|(_, code)| Self::Int(code))
    }
    fn parse_nocs(input: &mut &[u8]) -> PResult<Self> {
        let mut p = Self::NoCs.get_discriminant();
        p.parse_next(input).map(|_| Self::NoCs)
    }
    fn parse_li8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Li8(Regcode::END, 0).get_discriminant();
        (p, cut_err(Regcode::parse_next), cut_err(u8))
            .parse_next(input)
            .map(|(_, reg, imm)| Self::Li8(reg, imm))
    }
    fn parse_li16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Li16(Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, reg, imm)| Self::Li16(reg, imm))
    }
    fn parse_li32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Li32(Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, reg, imm)| Self::Li32(reg, imm))
    }
    fn parse_li64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Li64(Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, reg, imm)| Self::Li64(reg, imm))
    }
    fn parse_li128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Li128(Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, reg, imm)| Self::Li128(reg, imm))
    }
    fn parse_jmp(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jmp(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jmp(imm))
    }
    fn parse_jne(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jne(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jne(imm))
    }
    fn parse_jeq(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jeq(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jeq(imm))
    }
    fn parse_jlt(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jlt(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jlt(imm))
    }
    fn parse_jge(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jge(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jge(imm))
    }
    fn parse_jltu(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jltu(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jltu(imm))
    }
    fn parse_jgeu(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jgeu(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jgeu(imm))
    }
    fn parse_jez(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jez(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jez(imm))
    }
    fn parse_jnz(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Jnz(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Jnz(imm))
    }
    fn parse_call(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Call(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::Call(imm))
    }
    fn parse_syscall(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::SysCall(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::SysCall(imm))
    }
    fn parse_nativcall(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::NativeCall(0).get_discriminant();
        (p, cut_err(u64(Endianness::Little)))
            .parse_next(input)
            .map(|(_, imm)| Self::NativeCall(imm))
    }
    fn parse_not(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Not(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::Not(dst, src))
    }
    fn parse_or(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Or(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            Regcode::parse_next,
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Or(dst, src1, src2))
    }
    fn parse_and(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::And(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::And(dst, src1, src2))
    }
    fn parse_xor(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Xor(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Xor(dst, src1, src2))
    }
    fn parse_shl(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shl(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Shl(dst, src1, src2))
    }
    fn parse_shr(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shr(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Shr(dst, src1, src2))
    }
    fn parse_ori8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Ori8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Ori8(dst, src, imm))
    }
    fn parse_ori16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Ori16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Ori16(dst, src, imm))
    }
    fn parse_ori32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Ori32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Ori32(dst, src, imm))
    }
    fn parse_ori64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Ori64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Ori64(dst, src, imm))
    }
    fn parse_ori128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Ori128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Ori128(dst, src, imm))
    }
    fn parse_andi8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Andi8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Andi8(dst, src, imm))
    }
    fn parse_andi16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Andi16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Andi16(dst, src, imm))
    }
    fn parse_andi32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Andi32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Andi32(dst, src, imm))
    }
    fn parse_andi64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Andi64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Andi64(dst, src, imm))
    }
    fn parse_andi128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Andi128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Andi128(dst, src, imm))
    }
    fn parse_xori8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Xori8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            Regcode::parse_next,
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Xori8(dst, src, imm))
    }
    fn parse_xori16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Xori16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Xori16(dst, src, imm))
    }
    fn parse_xori32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Xori32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Xori32(dst, src, imm))
    }
    fn parse_xori64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Xori64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Xori64(dst, src, imm))
    }
    fn parse_xori128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Xori128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Xori128(dst, src, imm))
    }
    fn parse_shli8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shli8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shli8(dst, src, imm))
    }
    fn parse_shli16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shli16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shli16(dst, src, imm))
    }
    fn parse_shli32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shli32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shli32(dst, src, imm))
    }
    fn parse_shli64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shli64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shli64(dst, src, imm))
    }
    fn parse_shli128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shli128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shli128(dst, src, imm))
    }
    fn parse_shri8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shri8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shri8(dst, src, imm))
    }
    fn parse_shri16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shri16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shri16(dst, src, imm))
    }
    fn parse_shri32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shri32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shri32(dst, src, imm))
    }
    fn parse_shri64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shri64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shri64(dst, src, imm))
    }
    fn parse_shri128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Shri128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Shri128(dst, src, imm))
    }
    fn parse_add(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Add(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Add(dst, src1, src2))
    }
    fn parse_sub(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Sub(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Sub(dst, src1, src2))
    }
    fn parse_mul(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Mul(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Mul(dst, src1, src2))
    }
    fn parse_div(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Div(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Div(dst, src1, src2))
    }
    fn parse_mod(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Mod(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Mod(dst, src1, src2))
    }
    fn parse_addi8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Addi8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Addi8(dst, src, imm))
    }
    fn parse_addi16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Addi16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Addi16(dst, src, imm))
    }
    fn parse_addi32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Addi32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Addi32(dst, src, imm))
    }
    fn parse_addi64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Addi64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Addi64(dst, src, imm))
    }
    fn parse_addi128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Addi128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Addi128(dst, src, imm))
    }
    fn parse_subi8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Subi8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Subi8(dst, src, imm))
    }
    fn parse_subi16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Subi16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Subi16(dst, src, imm))
    }
    fn parse_subi32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Subi32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Subi32(dst, src, imm))
    }
    fn parse_subi64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Subi64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Subi64(dst, src, imm))
    }
    fn parse_subi128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Subi128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Subi128(dst, src, imm))
    }
    fn parse_muli8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Muli8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Muli8(dst, src, imm))
    }
    fn parse_muli16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Muli16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Muli16(dst, src, imm))
    }
    fn parse_muli32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Muli32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Muli32(dst, src, imm))
    }
    fn parse_muli64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Muli64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Muli64(dst, src, imm))
    }
    fn parse_muli128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Muli128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Muli128(dst, src, imm))
    }
    fn parse_divi8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Divi8(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u8),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Divi8(dst, src, imm))
    }
    fn parse_divi16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Divi16(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u16(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Divi16(dst, src, imm))
    }
    fn parse_divi32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Divi32(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u32(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Divi32(dst, src, imm))
    }
    fn parse_divi64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Divi64(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u64(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Divi64(dst, src, imm))
    }
    fn parse_divi128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Divi128(Regcode::END, Regcode::END, 0).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(u128(Endianness::Little)),
        )
            .parse_next(input)
            .map(|(_, dst, src, imm)| Self::Divi128(dst, src, imm))
    }
    fn parse_ld8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::LD8(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::LD8(dst, src))
    }
    fn parse_sv8(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::SV8(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::SV8(dst, src))
    }
    fn parse_ld16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::LD16(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::LD16(dst, src))
    }
    fn parse_sv16(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::SV16(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::SV16(dst, src))
    }
    fn parse_ld32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::LD32(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::LD32(dst, src))
    }
    fn parse_sv32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::SV32(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::SV32(dst, src))
    }
    fn parse_ld64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::LD64(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::LD64(dst, src))
    }
    fn parse_sv64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::SV64(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::SV64(dst, src))
    }
    fn parse_ld128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::LD128(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::LD128(dst, src))
    }
    fn parse_sv128(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::SV128(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::SV128(dst, src))
    }
    fn parse_addf32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Addf32(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Addf32(dst, src1, src2))
    }
    fn parse_addf64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Addf64(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Addf64(dst, src1, src2))
    }
    fn parse_subf32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Subf32(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Subf32(dst, src1, src2))
    }
    fn parse_subf64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Subf64(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Subf64(dst, src1, src2))
    }
    fn parse_mulf32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Mulf32(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Mulf32(dst, src1, src2))
    }
    fn parse_mulf64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Mulf64(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Mulf64(dst, src1, src2))
    }
    fn parse_divf32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Divf32(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Divf32(dst, src1, src2))
    }
    fn parse_divf64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Divf64(Regcode::END, Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2)| Self::Divf64(dst, src1, src2))
    }
    fn parse_fmadd32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Fmadd32(Regcode::END, Regcode::END, Regcode::END, Regcode::END)
            .get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2, src3)| Self::Fmadd32(dst, src1, src2, src3))
    }
    fn parse_fmadd64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Fmadd32(Regcode::END, Regcode::END, Regcode::END, Regcode::END)
            .get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src1, src2, src3)| Self::Fmadd64(dst, src1, src2, src3))
    }
    fn parse_itf64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::ITF64(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::ITF64(dst, src))
    }
    fn parse_itf32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::ITF32(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::ITF32(dst, src))
    }
    fn parse_fti64(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::FTI64(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::FTI64(dst, src))
    }
    fn parse_fti32(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::FTI32(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, dst, src)| Self::FTI32(dst, src))
    }
    fn parse_eq0(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Eq0(Regcode::END).get_discriminant();
        (p, cut_err(Regcode::parse_next))
            .parse_next(input)
            .map(|(_, src)| Self::Eq0(src))
    }
    fn parse_cmp(input: &mut &[u8]) -> PResult<Self> {
        let p = Self::Cmp(Regcode::END, Regcode::END).get_discriminant();
        (
            p,
            cut_err(Regcode::parse_next),
            cut_err(Regcode::parse_next),
        )
            .parse_next(input)
            .map(|(_, src1, src2)| Self::Cmp(src1, src2))
    }
}

impl ByteParser for Instruction {
    fn parse_next(input: &mut &[u8]) -> winnow::prelude::PResult<Self>
    where
        Self: Sized,
    {
        alt((
            Self::parse_noop_bytes,
            Self::parse_yield_bytes,
            Self::parse_ret,
            Self::parse_sched,
            Self::parse_int,
            Self::parse_nocs,
            Self::parse_li8,
            Self::parse_li16,
            Self::parse_li32,
            Self::parse_li64,
            Self::parse_li128,
            Self::parse_jmp,
            Self::parse_jne,
            Self::parse_jeq,
            Self::parse_jlt,
            Self::parse_call,
            Self::parse_syscall,
            Self::parse_nativcall,
            Self::parse_not,
            Self::parse_or,
            alt((
                Self::parse_and,
                Self::parse_xor,
                Self::parse_shl,
                Self::parse_shr,
                Self::parse_ori8,
                Self::parse_ori16,
                Self::parse_ori32,
                Self::parse_ori64,
                Self::parse_ori128,
                Self::parse_andi8,
                Self::parse_andi16,
                Self::parse_andi32,
                Self::parse_andi64,
                Self::parse_andi128,
                Self::parse_xori8,
                Self::parse_xori16,
                Self::parse_xori32,
                Self::parse_xori64,
                Self::parse_xori128,
                alt((
                    Self::parse_shli8,
                    Self::parse_shli16,
                    Self::parse_shli32,
                    Self::parse_shli64,
                    Self::parse_shli128,
                    Self::parse_shri8,
                    Self::parse_shri16,
                    Self::parse_shri32,
                    Self::parse_shri64,
                    Self::parse_shri128,
                    Self::parse_add,
                    Self::parse_sub,
                    Self::parse_mul,
                    Self::parse_div,
                    Self::parse_mod,
                    Self::parse_addi8,
                    Self::parse_addi16,
                    Self::parse_addi32,
                    Self::parse_addi64,
                    Self::parse_addi128,
                    alt((
                        Self::parse_subi8,
                        Self::parse_subi16,
                        Self::parse_subi32,
                        Self::parse_subi64,
                        Self::parse_subi128,
                        Self::parse_muli8,
                        Self::parse_muli16,
                        Self::parse_muli32,
                        Self::parse_muli64,
                        Self::parse_muli128,
                        Self::parse_divi8,
                        Self::parse_divi16,
                        Self::parse_divi32,
                        Self::parse_divi64,
                        Self::parse_divi128,
                        alt((
                            Self::parse_ld8,
                            Self::parse_ld16,
                            Self::parse_ld32,
                            Self::parse_ld64,
                            Self::parse_ld128,
                            Self::parse_sv8,
                            Self::parse_sv16,
                            Self::parse_sv32,
                            Self::parse_sv64,
                            Self::parse_sv128,
                            Self::parse_addf32,
                            Self::parse_addf64,
                            Self::parse_subf32,
                            Self::parse_subf64,
                            Self::parse_mulf32,
                            Self::parse_mulf64,
                            Self::parse_divf32,
                            Self::parse_divf64,
                            Self::parse_fmadd32,
                            Self::parse_fmadd64,
                            alt((
                                Self::parse_itf64,
                                Self::parse_itf32,
                                Self::parse_fti64,
                                Self::parse_fti32,
                                Self::parse_eq0,
                                Self::parse_cmp,
                            )),
                        )),
                    )),
                )),
            )),
        ))
        .parse_next(input)
    }
}
#[cfg(test)]
mod test {
    use crate::parser::{reg_code::Regcode, ByteParser};

    use super::Instruction;

    #[test]
    fn test1() {
        let code = Instruction::Li8(Regcode::END, 0).get_discriminant();
        assert_eq!(0x06, code);
    }

    #[test]
    fn parse_bytecode() {
        //let mut bytecode : &[u8] = &[0x6,0x2D, 0xad, 0x3c, 0x2d, 0x2d, 0x00, 0xde];
        let mut bytecode: &[u8] = b"\x06\x2D\xAD\x3C\x2D\x2D\x00\xDE";
        let bytes = &mut bytecode;
        let fst = Instruction::parse_next(bytes);
        assert_eq!(Ok(Instruction::Li8(Regcode::T0, 0xAD)), fst);
        let snd = Instruction::parse_next(bytes);
        assert_eq!(
            Ok(Instruction::Addi16(Regcode::T0, Regcode::T0, 0xDE00)),
            snd
        );
    }
}
