use winnow::{ascii::*, combinator::*, error::*, prelude::*};

fn ws<'a, F, O, E: ParserError<&'a str>>(inner: F) -> impl Parser<&'a str, O, E>
where
    F: Parser<&'a str, O, E>,
{
    delimited(multispace0, inner, multispace0)
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum SpecialSym {
    LastSegStart, //$
    LastSegEnd,   //# Last seg start + curr seg size.
    CurrSegSize,  //% until you finish the segment declaration, you are still within "curr segment"
    LinkTime,     //L
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Macro<'a> {
    Special(SpecialSym),
    Symbol(&'a str),
    Num(i128),
    SubExpr(Box<Self>),
    Add(Box<Self>, Box<Self>),
    Sub(Box<Self>, Box<Self>),
    Mul(Box<Self>, Box<Self>),
    Div(Box<Self>, Box<Self>),
    Mod(Box<Self>, Box<Self>),
    And(Box<Self>, Box<Self>),
    Or(Box<Self>, Box<Self>),
    Xor(Box<Self>, Box<Self>),
    Lsh(Box<Self>, Box<Self>),
    Rsh(Box<Self>, Box<Self>),
    Neg(Box<Self>),
    Not(Box<Self>),
}

impl<'a> Macro<'a> {
    fn symbol_p(input: &mut &'a str) -> PResult<Self> {
        (
            alt((alpha1, "_")),
            repeat::<_, _, (), _, _>(0.., alt((alphanumeric1, "_"))),
        )
            .recognize()
            .map(Self::Symbol)
            .parse_next(input)
    }
    fn special_p(input: &mut &'a str) -> PResult<Self> {
        ('\\', alt(('#', '%', '$', 'L')))
            .recognize()
            .map(|x| match x {
                "\\$" => Self::Special(SpecialSym::LastSegStart),
                "\\%" => Self::Special(SpecialSym::CurrSegSize),
                "\\#" => Self::Special(SpecialSym::LastSegEnd),
                "\\L" => Self::Special(SpecialSym::LinkTime),
                _ => unreachable!(),
            })
            .parse_next(input)
    }

    fn num_p(input: &mut &'a str) -> PResult<Self> {
        alt((
            preceded(
                "0o",
                cut_err(alphanumeric1.try_map(|s| i128::from_str_radix(s, 8))),
            )
            .context(StrContext::Label(
                ": not a valid octal number; Octal number's digits are contained in 0-7.",
            )),
            preceded(
                "0b",
                cut_err(alphanumeric1.try_map(|s| i128::from_str_radix(s, 2))),
            )
            .context(StrContext::Label(
                ": not a valid binary number; Binary numbers are made solely of 1s and 0s",
            )),
            preceded(
                "0x",
                cut_err(alphanumeric1.try_map(|s| i128::from_str_radix(s, 16))),
            )
            .context(
                    StrContext::Label(
                        ": not a valid hexadecimal number; Hexadecimal's digits are contained withtin the domain 0-(f|F).")),
            digit1.try_map(|s| i128::from_str_radix(s, 10)),
        ))
        .map(Self::Num)
        .parse_next(input)
    }

    fn neg_p(input: &mut &'a str) -> PResult<Self> {
        let (_, right) = (ws('~'), Self::priority_highest_p).parse_next(input)?;
        Ok(Self::Neg(Box::new(right)))
    }
    fn not_p(input: &mut &'a str) -> PResult<Self> {
        let (_, right) = (ws('!'), Self::priority_highest_p).parse_next(input)?;
        Ok(Self::Not(Box::new(right)))
    }

    fn priority_lowest_p(input: &mut &'a str) -> PResult<Self> {
        // Add, Sub, Mod
        let (mut left, rest) = (
            Self::priority_lower_p,
            repeat::<_, _, Vec<(char, Self)>, _, _>(
                0..,
                (ws(alt(('+', '-', '%'))), Self::priority_lower_p),
            ),
        )
            .parse_next(input)?;
        for (ch, m) in rest {
            match ch {
                '+' => left = Self::Add(Box::new(left), Box::new(m)),
                '-' => left = Self::Sub(Box::new(left), Box::new(m)),
                '%' => left = Self::Mod(Box::new(left), Box::new(m)),
                _ => unreachable!(),
            }
        }
        Ok(left)
    }
    fn priority_lower_p(input: &mut &'a str) -> PResult<Self> {
        // Mul, Div
        let (mut left, rest) = (
            Self::priority_mid_p,
            repeat::<_, _, Vec<(char, Self)>, _, _>(
                0..,
                (ws(alt(('*', '/'))), Self::priority_mid_p),
            ),
        )
            .parse_next(input)?;
        for (ch, m) in rest {
            match ch {
                '*' => left = Self::Mul(Box::new(left), Box::new(m)),
                '/' => left = Self::Div(Box::new(left), Box::new(m)),
                _ => unreachable!(),
            }
        }
        Ok(left)
    }
    fn priority_mid_p(input: &mut &'a str) -> PResult<Self> {
        // Binary ops
        let (mut left, rest) = (
            Self::priority_higher_p,
            repeat::<_, _, Vec<(&str, Self)>, _, _>(
                0..,
                (
                    ws(alt(("|", "&", ">>", "<<", "^"))),
                    Self::priority_higher_p,
                ),
            ),
        )
            .parse_next(input)?;
        for (ch, m) in rest {
            match ch {
                "|" => left = Self::Or(Box::new(left), Box::new(m)),
                "&" => left = Self::And(Box::new(left), Box::new(m)),
                "^" => left = Self::Xor(Box::new(left), Box::new(m)),
                ">>" => left = Self::Rsh(Box::new(left), Box::new(m)),
                "<<" => left = Self::Lsh(Box::new(left), Box::new(m)),
                _ => unreachable!(),
            }
        }
        Ok(left)
    }
    fn priority_higher_p(input: &mut &'a str) -> PResult<Self> {
        // Not, Neg
        alt((Self::not_p, Self::neg_p, Self::priority_highest_p)).parse_next(input)
    }
    fn priority_highest_p(input: &mut &'a str) -> PResult<Self> {
        // Num, Symbol
        alt((
            Self::symbol_p,
            Self::num_p,
            Self::special_p,
            ws(Self::expr),
            //Self::priority_lowest_p,
        ))
        .parse_next(input)
    }

    fn expr(input: &mut &'a str) -> PResult<Self> {
        delimited(
            "$(",
            preceded(multispace0, Self::priority_lowest_p),
            preceded(multispace0, cut_err(")")),
        )
        .map(|x| Macro::SubExpr(Box::new(x)))
        .context(StrContext::Label(": unmatched parens"))
        .parse_next(input)
    }

    pub fn parse_all(input: &mut &'a str) -> Result<Self, String> {
        ws(Self::priority_lowest_p)
            .parse(input)
            .map_err(|e| e.to_string())
    }
    pub fn parse(input: &mut &'a str) -> PResult<Self> {
        ws(Self::priority_lowest_p)
            .context(StrContext::Label("Failed to parse unknown token"))
            .parse_next(input)
    }
}

#[cfg(test)]
mod testing {
    use crate::parser::macro_expr::SpecialSym;

    use super::Macro;

    #[test]
    fn test_1() {
        //let test_str = "$(1 + 3 * 56 + Hell)";
        let mut test_str = " 1 *~$(3 + 56) * Hell";
        let res = Macro::parse(&mut test_str);
        if res.is_err() {
            assert!(false, "{}", res.unwrap_err());
        }
        assert_eq!(
            res,
            Ok(Macro::Mul(
                Box::new(Macro::Mul(
                    Box::new(Macro::Num(1)),
                    Box::new(Macro::Neg(Box::new(Macro::SubExpr(Box::new(Macro::Add(
                        Box::new(Macro::Num(3)),
                        Box::new(Macro::Num(56))
                    ))))))
                )),
                Box::new(Macro::Symbol("Hell"))
            ))
        );
    }
    #[test]
    fn test_2() {
        let mut test_str = "\\# + $(30+ 0x5678 * $(~_1OL / $(h1_hi)));";
        let res = Macro::parse(&mut test_str);
        if res.is_err() {
            assert!(false, "{}", res.unwrap_err());
        }
        assert_eq!(
            res,
            Ok(Macro::Add(
                Box::new(Macro::Special(SpecialSym::LastSegEnd)),
                Box::new(Macro::SubExpr(Box::new(Macro::Add(
                    Box::new(Macro::Num(30)),
                    Box::new(Macro::Mul(
                        Box::new(Macro::Num(22136)),
                        Box::new(Macro::SubExpr(Box::new(Macro::Div(
                            Box::new(Macro::Neg(Box::new(Macro::Symbol("_1OL")))),
                            Box::new(Macro::SubExpr(Box::new(Macro::Symbol("h1_hi"))))
                        ))))
                    ))
                ))))
            ))
        );
        assert_eq!(test_str, ";");
        //assert!(false, "{:#?}", res);
    }
}
